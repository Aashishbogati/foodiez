//
//  BaseCollectionViewCell.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/26/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class BaseCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        configViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configViews()
    }
    
    func configViews() {
        
    }
}
