//
//  UIViewController+Ext.swift
//  Foodiez
//
//  Created by Aashish Bogati on 4/28/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

extension UIViewController {
    func showEmptyStateUser() {
        let emptyStateView = EmptyStateUser()
        emptyStateView.frame = view.bounds
        if view.subviews.contains(emptyStateView) {
            view.bringSubviewToFront(emptyStateView)
        } else {
            view.addSubview(emptyStateView)
        }
        
    }
    
    func removeEmptyStateUser() {
        view.subviews.forEach { (subView) in
            if subView is EmptyStateUser {
                subView.removeFromSuperview()
            }
        }
    }
}
