//
//  SetupAttributedTextButton.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/25/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class SetupAttributedText {
    static var shared = SetupAttributedText()
    
    func setupButton(firstString: String?,secondString: String?,buttonName: UIButton?) {
        let attributedText = NSMutableAttributedString(string: firstString!, attributes: [NSAttributedString.Key.font:  UIFont.boldSystemFont(ofSize: 14),NSAttributedString.Key.foregroundColor : ColorCompatibility.secondaryLabel])
        
        let attributedSubText = NSMutableAttributedString(string: secondString!, attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14),NSAttributedString.Key.foregroundColor: Color.defaultColor])
        
        attributedText.append(attributedSubText)
        buttonName?.setAttributedTitle(attributedText, for: .normal)
    }
    
    func setupText(firstString: String?,secondString: String?,labelName: UILabel?,firstTextColor: UIColor?,subTextColor: UIColor?,textAlignment: NSTextAlignment?,firstTextFontSize: Float?,subTextFontSize:Float?) {
        let attributedText = NSMutableAttributedString(string: firstString!, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: CGFloat(firstTextFontSize!), weight: UIFont.Weight(rawValue: 400)),NSAttributedString.Key.foregroundColor : firstTextColor!])
        
        let attributedSubText = NSMutableAttributedString(string: secondString!, attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: CGFloat(subTextFontSize!)),NSAttributedString.Key.foregroundColor: subTextColor!])
        
        attributedText.append(attributedSubText)
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 0
        attributedText.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attributedText.length))
        labelName?.attributedText = attributedText
        labelName?.numberOfLines = 0
        labelName?.textAlignment = textAlignment!
        
    }
}
