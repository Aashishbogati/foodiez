//
//  UIStoryboard+Storyboards.swift
//  Veda Guru
//
//  Created by Aashish Bogati on 4/15/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit
extension UIViewController {

    class func instantiate<T: UIViewController>(name: String) -> T {

        let storyboard = UIStoryboard(name: name, bundle: nil)
        let identifier = String(describing: self)
        return storyboard.instantiateViewController(withIdentifier: identifier) as! T
    }
}
