//
//  Model.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/29/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import Foundation

struct Cuisine {
    let name : String?
    let bgImage : String?
}

struct HeroContent {
    let thumbImage : String?
    let name : String?
    let numberOfPlace : String?
}

struct MostPopular: Decodable {
    let status : Bool?
    let message : String?
    let data : [MostPopularRes?]
}

struct MostPopularRes : Decodable {
    let restaurant_id : Int?
    let restaurnat_name : String?
    let address : String?
    let food_image : String?
    let food_name : String?
}

struct MealDeal : Decodable {
    let status : Bool?
    let message : String?
    let data: [MealDeals?]
}

struct MealDeals : Decodable {
    let food_name : String?
    let food_image : String?
//    let restaurants : [Restaurants_data?]
}



struct Photos {
    let thumbImage : String?
}

