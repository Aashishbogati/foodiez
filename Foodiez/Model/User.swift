//
//  User.swift
//  Foodiez
//
//  Created by Aashish Bogati on 4/28/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import Foundation

struct User : Codable {
    let status : Bool?
    let message : String?
    let data : UserData?
}

struct UserData : Codable {
    let firstname : String?
    let lastname : String?
    let username : String?
    let email : String?
    let address : String?
    let image : String?
}
