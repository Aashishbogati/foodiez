//
//  Menu.swift
//  Foodiez
//
//  Created by Aashish Bogati on 4/27/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import Foundation


struct Menu : Decodable {
    let status : Bool?
    let message : String?
    let data : MenuData?
}

struct MenuData : Decodable {
    let restaurant_id : Int?
    let items : [MenuItems?]
}

struct MenuItems : Decodable {
    let food_image : String?
    let food_name : String?
    let price : Int?
}
