//
//  Restaurant.swift
//  Foodiez
//
//  Created by Aashish Bogati on 4/27/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import Foundation

struct Restaurants : Decodable {
    let status : Bool?
    let data : [Restaurants_data?]
}

struct Restaurants_data : Decodable {
    let restaurant_id : Int?
    let restaurant_name : String?
    let address : String?
    let restaurant_image : String?
}
