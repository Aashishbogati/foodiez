//
//  Reviews.swift
//  Foodiez
//
//  Created by Aashish Bogati on 4/27/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import Foundation

struct Reviews : Decodable {
    let status : Bool?
    let data : Data?
    
}

struct Data: Decodable {
    let restaurant_id : Int?
    let reviews : [ReviewsData?]
}

struct ReviewsData: Decodable {
    let name : String?
    let comment :  String?
}
