//
//  NetworkManger.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/29/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit
enum JSONError: String,Error {
    case NoData = "ERROR: no data"
    case ConversionFailed = "ERROR: conversion from JSON failed"
}
class NetworkManger {
    static var shared = NetworkManger()
    
    func fetchData<T: Decodable>(urlString: String, method : String, completion: @escaping (T) -> ()) {
        guard let url = URL(string: urlString) else { return }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method
        urlRequest.addValue("12345", forHTTPHeaderField: "X-Api-Key")
        URLSession.shared.dataTask(with: urlRequest) { (data, resp, err) in
            
            if let err = err {
                print("Failed to fetch data:", err)
                return
            }
            
            guard let data = data else { return }
            
            do {
//                self.progressView.hideAnimation()
                let obj = try JSONDecoder().decode(T.self, from: data)
                completion(obj)
            } catch let jsonErr {
                print("Failed to decode json:", jsonErr)
            }
        }.resume()
    }
    
    func postData<T: Codable>(urlString : String, method : String, params : [String: Any], completion: @escaping (T) -> ()) {
        guard let url = URL(string: urlString) else { return }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method
        urlRequest.addValue("12345", forHTTPHeaderField: "X-Api-Key")
        do {
            let userData = try JSONSerialization.data(withJSONObject: params, options: .init())
            urlRequest.httpBody = userData
            URLSession.shared.dataTask(with: urlRequest) { (data, resp, err) in
                if let err = err {
                    print("Failed to fetch data:",err)
                    return
                }
                
                guard let postData = data else { return }
                
                do {
                    let obj = try JSONDecoder().decode(T.self, from: postData)
                    guard let json = try JSONSerialization.jsonObject(with: postData, options: []) as? NSDictionary else {
                        throw JSONError.ConversionFailed
                    }
                    UserDefaults.standard.set(json, forKey: "User")
                    completion(obj)
                } catch let jsonErr {
                    print("Failed to decode json:",jsonErr)
                }
            }.resume()
            
        } catch let jsonErr {
            print("Failed to decode json:", jsonErr)
        }
        
    }


}

