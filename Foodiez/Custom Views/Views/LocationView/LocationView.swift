//
//  LocationView.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/30/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class LocationView: UIView {
    
    //MARK:-
    //MARK:- PROPERTIES
    var blackView = UIView()
    var contentView = UIView()
    
    let titleLabel : TitleLabel = {
        let label = TitleLabel()
        label.textColor = ColorCompatibility.label
        label.text = "Search Location"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let dismissButton : UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "cancel")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btn.tintColor = ColorCompatibility.secondaryLabel
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let searchBarCTR: UISearchController = {
        let searchCtr = UISearchController()
        searchCtr.searchBar.placeholder = "Search for your location"
        searchCtr.searchBar.translatesAutoresizingMaskIntoConstraints = false
        return searchCtr
    }()
    
    let locationButton : UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "marker")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btn.setTitle("Use Current Location", for: .normal)
        btn.titleLabel?.font = .boldSystemFont(ofSize: 14)
        btn.setTitleColor(Color.defaultColor, for: .normal)
        btn.tintColor = Color.defaultColor
        btn.contentHorizontalAlignment = .left
        btn.contentEdgeInsets = .init(top: 0, left: 10, bottom: 0, right: 0)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let addAddressButton : UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "plus")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btn.setTitle("Add Address", for: .normal)
        btn.titleLabel?.font = .boldSystemFont(ofSize: 14)
        btn.setTitleColor(Color.defaultColor, for: .normal)
        btn.tintColor = Color.defaultColor
        btn.contentHorizontalAlignment = .left
        btn.contentEdgeInsets = .init(top: 0, left: 10, bottom: 0, right: 0)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let sepratorLine : UIView = {
        let sl = UIView()
        sl.backgroundColor = ColorCompatibility.secondaryLabel
        sl.translatesAutoresizingMaskIntoConstraints = false
        return sl
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
//        searchBarCTR.definesPresentationContext = true
//        searchBarCTR.extendedLayoutIncludesOpaqueBars = true
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
    }
    
    
    
    //MARK:-
    //MARK:- FUNCTIONS
    
    func configContentView() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(dismissButton)
        contentView.addSubview(searchBarCTR.searchBar)
        contentView.addSubview(locationButton)
        contentView.addSubview(addAddressButton)
        
        NSLayoutConstraint.activate([
            
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 10),
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant: 10),
            titleLabel.heightAnchor.constraint(equalToConstant: 30),
            
            dismissButton.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 10),
            dismissButton.leadingAnchor.constraint(equalTo: titleLabel.trailingAnchor),
            dismissButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor,constant: -10),
            
            searchBarCTR.searchBar.topAnchor.constraint(equalTo: titleLabel.bottomAnchor,constant: 10),
            searchBarCTR.searchBar.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            searchBarCTR.searchBar.trailingAnchor.constraint(equalTo: contentView.trailingAnchor,constant: -10),
            
            locationButton.topAnchor.constraint(equalTo: searchBarCTR.searchBar.bottomAnchor,constant: 10),
            locationButton.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            locationButton.trailingAnchor.constraint(equalTo: dismissButton.trailingAnchor),
            
            addAddressButton.topAnchor.constraint(equalTo: locationButton.bottomAnchor,constant: 10),
            addAddressButton.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            addAddressButton.trailingAnchor.constraint(equalTo: dismissButton.trailingAnchor),
            addAddressButton.heightAnchor.constraint(equalToConstant: 50),
        ])
    }
    
    func showSetting() {
        if let window = UIApplication.shared.keyWindow {
            self.blackView.backgroundColor = UIColor.init(white: 0, alpha: 0.5)
            let tap = UITapGestureRecognizer(target: self, action: #selector(dismissVC))
            self.blackView.addGestureRecognizer(tap)
            window.addSubview(blackView)
            window.addSubview(contentView)
            
            self.blackView.frame = window.frame
            
            let y = window.frame.height - window.frame.height / 1.2

            contentView.backgroundColor = .white
            contentView.frame = .init(x: 0, y: window.frame.height, width: window.frame.width, height: window.frame.height)
            self.blackView.alpha = 0
            UIView.animate(withDuration: 0.5) {
                self.blackView.alpha = 1
                self.contentView.frame = .init(x: 0, y: y, width: self.contentView.frame.width, height: self.contentView.frame.height)
            }
            dismissButton.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
            searchBarCTR.definesPresentationContext = true
            searchBarCTR.extendedLayoutIncludesOpaqueBars = false
            
            configContentView()
        }
    }
    
    //MARK:-
    //MARK:- ACTIONS
    @objc func dismissVC() {
        UIView.animate(withDuration: 0.5) {
            self.blackView.alpha = 0
            
            if let window = UIApplication.shared.keyWindow {
                self.contentView.frame = CGRect(x: 0, y: window.frame.height, width: self.contentView.frame.width, height: self.contentView.frame.height)
            }
        }
        
    }
    
}
