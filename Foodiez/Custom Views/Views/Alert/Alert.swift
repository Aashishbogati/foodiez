//
//  Alert.swift
//  Foodiez
//
//  Created by Aashish Bogati on 4/29/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

struct Alert {
    static func showAlert(on vc : UIViewController,title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok", style: .cancel, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
    
    static func showLogutAlert(on vc: UIViewController, title : String, message: String, handler: ((UIAlertAction) -> ())? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "cancle", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Logout", style: .destructive, handler: handler))
        vc.present(alert,animated: true,completion: nil)
    }
    
    
}
