//
//  ProgressView.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/29/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit
import Lottie

open class ProgressView {
    var progressView = UIView()
    var animationView = AnimationView()
    
    open func showAnimation() {
        let window = UIWindow(frame: UIScreen.main.bounds)
        progressView.frame = .init(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
        progressView.backgroundColor = .white
        progressView.clipsToBounds = true
        
        
        animationView.frame = .init(x: 0, y: 0, width: 300, height: 300)
        animationView.center = self.progressView.center
        animationView.contentMode = .scaleAspectFit
        
        UIApplication.shared.keyWindow?.addSubview(progressView)
        progressView.addSubview(animationView)
        animationView.animation = Animation.named("loading")
        animationView.play()
        animationView.loopMode = .loop
        
        
    }
    
    open func hideAnimation() {
        DispatchQueue.main.async {
            self.progressView.removeFromSuperview()
        }
    }
}
