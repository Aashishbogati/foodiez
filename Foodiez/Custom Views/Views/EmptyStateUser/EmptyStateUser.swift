//
//  EmptyStateUser.swift
//  Foodiez
//
//  Created by Aashish Bogati on 4/28/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class EmptyStateUser: UIView {
    
    //MARK:-
    //MARK:- PROPERTIES
    let loginButton : UIButton = {
        let button = UIButton()
        button.setTitle("Continue with Email", for: .normal)
        button.backgroundColor = Color.defaultColor
        button.setTitleColor(.white, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(loginButtonAction), for: .touchUpInside)
        return button
    }()
    
    let loginWithFacebook : UIButton = {
        let button = UIButton()
        button.setTitle("Continue with Facebook", for: .normal)
        button.backgroundColor = Color.fbColor
        button.setTitleColor(.white, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(loginButtonAction), for: .touchUpInside)
        return button
    }()
    
    let loginWithTwitter : UIButton = {
        let button = UIButton()
        button.setTitle("Continue with Twitter", for: .normal)
        button.backgroundColor = Color.twitterColor
        button.setTitleColor(.white, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(loginButtonAction), for: .touchUpInside)
        return button
    }()
    
    let vstack : UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 20
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configViews()
        backgroundColor = .white
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configViews()
        backgroundColor = .white
    }
    
    //MARK:-
    //MARK:- FUNCTIONS
    func configViews() {
        addSubview(vstack)
        vstack.addArrangedSubview(loginButton)
        vstack.addArrangedSubview(loginWithFacebook)
        vstack.addArrangedSubview(loginWithTwitter)
        
        NSLayoutConstraint.activate([
            vstack.centerYAnchor.constraint(equalTo: centerYAnchor),
            vstack.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 12),
            vstack.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -12),
            vstack.heightAnchor.constraint(equalToConstant: 170)
        ])
    }
    
    @objc func loginButtonAction() {
        let loginVC : LoginViewController = LoginViewController.instantiate(name: "Login")
        let loginNC = UINavigationController(rootViewController: loginVC)
        loginNC.modalPresentationStyle = .overFullScreen
        loginNC.modalTransitionStyle = .crossDissolve
        self.window?.rootViewController?.present(loginNC, animated: true, completion: nil)
    }
    
}
