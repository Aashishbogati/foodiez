//
//  SlideInMenu.swift
//  Foodiez
//
//  Created by Aashish Bogati on 4/1/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class Settings: NSObject {
    let name: String
    let iconName: String
    
    init(name: String, iconName: String) {
        self.name = name
        self.iconName = iconName
    }
}

private let cellID = "ProfileSettingViewCell"
class SlideInMenu: UIView,CollectionViewMethod {
    override init(frame: CGRect) {
        super.init(frame: frame)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(SlideInMenuCell.self, forCellWithReuseIdentifier: cellID)
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- setting name icon setup
    let settings : [Settings] = {
        return [Settings(name: "Settings", iconName: "settings"), Settings(name: "Terms & privacy policy", iconName: "privacy"), Settings(name: "Send Feedback", iconName: "feedback"), Settings(name: "Help", iconName: "help"), Settings(name: "Cancel", iconName: "cancel-1")]
    }()
    
    //MARK:- collection view
    let collectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.isScrollEnabled = false
        cv.backgroundColor = .white
        return cv
    }()
    
    let blackView = UIView()
    let cellHeight : CGFloat = 50
    func showSettings() {
        if let window = UIApplication.shared.keyWindow {
            blackView.backgroundColor = UIColor.init(white: 0, alpha: 0.5)
            let tap = UITapGestureRecognizer(target: self, action: #selector(dismissView))
            blackView.addGestureRecognizer(tap)
            window.addSubview(blackView)
            window.addSubview(collectionView)

            blackView.frame = window.frame
            
            let height : CGFloat = CGFloat(settings.count) * cellHeight
            let y = window.frame.height - height
            collectionView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: height)
            blackView.alpha = 0
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.blackView.alpha = 1
                
                self.collectionView.frame = CGRect(x: 0, y: y, width: self.collectionView.frame.width, height: self.collectionView.frame.height)
            }, completion: nil)
        }
    }
    
    //MARK:- dismiss view
    @objc func dismissView() {
        UIView.animate(withDuration: 0.5) {
            self.blackView.alpha = 0
            
            if let window = UIApplication.shared.keyWindow {
                self.collectionView.frame = CGRect(x: 0, y: window.frame.height, width: self.collectionView.frame.width, height: self.collectionView.frame.height)
            }
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return settings.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if indexPath.item == 4 {
//            self.dismissView()
//            let alert = UIAlertController(title: "Logout", message: "Are you sure want to logut?", preferredStyle: .alert)
//            
//            alert.addAction(UIAlertAction(title: "Logout", style: .destructive, handler: { (actionSheetController) in
//                let loginVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
//                UIApplication.shared.keyWindow?.rootViewController = UINavigationController(rootViewController: loginVC)
//            }))
//            
//            
//            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//            UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
//        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! SlideInMenuCell
        cell.settings = settings[indexPath.row]
        return cell
    }
    
    
    //MARK:- size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: collectionView.frame.width, height: cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}



