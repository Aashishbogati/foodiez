//
//  ProfileSettingViewCell.swift
//  Fitness
//
//  Created by Aashish Bogati on 3/24/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class SlideInMenuCell: BaseCollectionViewCell {
    
    var settings : Settings? {
        didSet {
    
            if let settingName = settings?.name {
                settingLabel.text = settingName
            }
            
            if let iconName = settings?.iconName {
                settingIcon.image = UIImage(named: iconName)
            }
            
        }
    }
    
    let settingIcon : UIImageView = {
        let icon = UIImageView()
        icon.image = UIImage(named: "settings")?.withRenderingMode(.alwaysOriginal)
        icon.contentMode = .scaleAspectFill
        icon.translatesAutoresizingMaskIntoConstraints = false
        return icon
    }()

    let settingLabel : UILabel = {
        let label = UILabel()
        label.text = "Settings"
        label.font = .systemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    

    
    override func configViews() {
        
        addSubview(settingIcon)
        addSubview(settingLabel)
        
        NSLayoutConstraint.activate([
            settingIcon.centerYAnchor.constraint(equalTo: centerYAnchor),
            settingIcon.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 8),
            settingIcon.heightAnchor.constraint(equalToConstant: 30),
            settingIcon.widthAnchor.constraint(equalToConstant: 30),
            
            settingLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            settingLabel.leadingAnchor.constraint(equalTo: settingIcon.trailingAnchor,constant: 8),
            settingLabel.trailingAnchor.constraint(equalTo: trailingAnchor,constant: 8),
            
        ])

    }
}
