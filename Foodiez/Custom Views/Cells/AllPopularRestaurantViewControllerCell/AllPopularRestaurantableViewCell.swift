//
//  AllPopularRestaurantableViewCell.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/31/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit
import SDWebImage
class AllPopularRestaurantableViewCell: UITableViewCell {
    
    var popularRestaurant : Restaurants_data? {
           didSet {
               if let title = popularRestaurant?.restaurant_name {
                   titleLabel.text = title
               }
               
               if let place = popularRestaurant?.address {
                   placeLabel.text = place
               }
               if let thumbimage = popularRestaurant?.restaurant_image {
                   let imageURL = URL(string: thumbimage)
                   thumbImage.sd_setImage(with: imageURL, completed: nil)
               }
           }
       }
       
       @IBOutlet weak var titleLabel: SmallTitleLabel!
       @IBOutlet weak var placeLabel: SecondaryLabel!
       @IBOutlet weak var thumbImage: CornorRadiusImage!
       override func awakeFromNib() {
           super.awakeFromNib()
           // Initialization code
       }

       override func setSelected(_ selected: Bool, animated: Bool) {
           super.setSelected(selected, animated: animated)

           // Configure the view for the selected state
       }

}
