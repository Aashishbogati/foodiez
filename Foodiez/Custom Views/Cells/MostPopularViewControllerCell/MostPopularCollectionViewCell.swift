//
//  MostPopularCollectionViewCell.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/27/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit
import SDWebImage

class MostPopularCollectionViewCell: BaseCollectionViewCell {
    var mostPopular : MostPopularRes? {
        didSet {
            if let title = mostPopular?.food_name {
                self.titleLabel.text = title
            }
            if let resAddress = mostPopular?.address {
                addressLabel.text = resAddress
            }

            if let thumbimage = mostPopular?.food_image {
                let imageURL = URL(string: thumbimage)
                thumbImageVIew.sd_setImage(with: imageURL, completed: nil)
            }
        }
    }
    @IBOutlet weak var thumbImageVIew: CornorRadiusImage!
    @IBOutlet weak var titleLabel: SmallTitleLabel!
    @IBOutlet weak var addressLabel: SecondaryLabel!
    
}
