//
//  ReviewsCollectionViewCell.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/31/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class ReviewsCollectionViewCell: BaseCollectionViewCell {
    
    var review : ReviewsData? {
        didSet {
            if let username = review?.name {
                usernameLabel.text = username
            }
            
            if let comment = review?.comment {
                commentLabel.text = comment
            }
        }
    }

    @IBOutlet weak var usernameLabel: SmallTitleLabel!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var seperatorLine: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        let screenWidth = UIScreen.main.bounds.size.width
        widthConstraint.constant = screenWidth - 2
    }
    
    //MARK:-
    //MARK:- FUNCTIONS
    override func configViews() {
        if let spLine = seperatorLine {
            spLine.backgroundColor = ColorCompatibility.secondaryLabel
        }
        
    }

}
