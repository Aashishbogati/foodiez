//
//  MealDealsDetailTableViewCell.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/31/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit
import SDWebImage

class MealDealsDetailTableViewCell: UITableViewCell {
    var restaurant : Restaurants_data? {
        didSet {
            if let title = restaurant?.restaurant_name {
                titleLabel.text = title
            }
            
            if let place = restaurant?.address {
                addressLabel.text = place
            }
            if let thumbimage = restaurant?.restaurant_image {
                let imageURL = URL(string: thumbimage)
                thumbImage.sd_setImage(with: imageURL, completed: nil)
            }
//            if let ratingCount = restaurant?.reviews?.count {
//                ratingLabel.text = "(\(ratingCount) ratings)"
//            }
        }
        
    }
    @IBOutlet weak var titleLabel: SmallTitleLabel!
    @IBOutlet weak var thumbImage: CornorRadiusImage!
    @IBOutlet weak var addressLabel: SecondaryLabel!
    @IBOutlet weak var reviewImage: UIImageView! {
        didSet {
            reviewImage.image = UIImage(named: "star")?.withRenderingMode(.alwaysTemplate)
            reviewImage.tintColor = Color.defaultColor
        }
    }
    @IBOutlet weak var ratingLabel: SecondaryLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
