//
//  DealsCollectionViewCell.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/27/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit
import SDWebImage

class DealsCollectionViewCell: BaseCollectionViewCell {
    var mealDeal : MealDeals? {
        didSet {
            
            if let imageName = mealDeal?.food_image {
                let imageUrl = URL(string: imageName)
                thumbImageView.sd_setImage(with: imageUrl, completed: nil)
            }
            
            if let title = mealDeal?.food_name {
                titleLabel.text = title
            }
            
//            if let totalRes = mealDeal?.restaurants.count {
//                totalRestaurantsLabel.text = "\(totalRes) Restaurants"
//            }
        }
    }
    @IBOutlet weak var thumbImageView: CornorRadiusImage!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var placeLabel: UILabel!
}
