//
//  HeroSliderCollectionViewCell.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/26/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class HeroSliderCollectionViewCell: BaseCollectionViewCell {
    
    var heroContent : HeroContent? {
        didSet {
            if let image = heroContent?.thumbImage {
                thumbImageVIew.image = UIImage(named: image)
            }
            
            if let title = heroContent?.name {
                titleLabel.text = title
            }
            
            if let numberOfPlace = heroContent?.numberOfPlace {
                numberOfPlaceLabel.text = numberOfPlace
            }
        }
    }
    
    @IBOutlet weak var thumbImageVIew: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var numberOfPlaceLabel: UILabel!
}
