//
//  PopularCollectionViewCell.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/27/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit
import SDWebImage

class PopularCollectionViewCell: BaseCollectionViewCell {
    
    var popularRestaurant : MostPopularRes? {
        didSet {
            if let title = popularRestaurant?.food_name {
                titleLabel.text = title
            }
            
            if let place = popularRestaurant?.address {
                locationLabel.text = place
            }
            if let thumbimage = popularRestaurant?.food_image {
                let imageURL = URL(string: thumbimage)
                thumbImageView.sd_setImage(with: imageURL, completed: nil)
           }
         
        }
    }
    
    @IBOutlet weak var thumbImageView: CornorRadiusImage!
    @IBOutlet weak var titleLabel: SmallTitleLabel!
    @IBOutlet weak var locationLabel: SecondaryLabel!
}
