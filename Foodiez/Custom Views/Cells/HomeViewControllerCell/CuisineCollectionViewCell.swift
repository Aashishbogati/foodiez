//
//  CuisineCollectionViewCell.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/30/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class CuisineCollectionViewCell: BaseCollectionViewCell {
    var cuisine : Cuisine? {
        didSet {
            if let bgimage = cuisine?.bgImage {
                self.cuisineImage.image = UIImage(named: bgimage)
            }
            
            if let title = cuisine?.name {
                self.cusineTitleLabel.text = title
            }
        }
    }
    @IBOutlet weak var cuisineImage: CornorRadiusImage!
    @IBOutlet weak var cusineTitleLabel: UILabel!
    @IBOutlet weak var layoverView: UIView! {
        didSet {
            layoverView.layer.cornerRadius = 5
            layoverView.clipsToBounds = true
        }
    }
}
