//
//  MenuTableViewCell.swift
//  Foodiez
//
//  Created by Aashish Bogati on 4/27/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit
import SDWebImage

class MenuTableViewCell: UITableViewCell {
    var menu : MenuItems? {
        didSet {
            if let foodName = menu?.food_name {
                titleLabel.text = foodName
            }
            
            if let foodPrice = menu?.price {
                priceLabel.text = "Rs \(foodPrice)"
            }
            
            if let thumbImage = menu?.food_image {
                let imageURL = URL(string: thumbImage)
                foodImage.sd_setImage(with: imageURL, completed: nil)
            }
            
            
        }
    }
    
    //MARK:-
    //MARK:- PROPERTIES
    @IBOutlet weak var foodImage: UIImageView!
    @IBOutlet weak var titleLabel: TitleLabel!
    @IBOutlet weak var priceLabel: SecondaryLabel!
    @IBOutlet weak var plusButton: UIButton!
    
    //MARK:-
    //MARK:- LIFE CYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configPlusButton()
    }
    
    func configPlusButton() {
        plusButton.setImage(UIImage(named: "plus")?.withRenderingMode(.alwaysTemplate), for: .normal)
        plusButton.tintColor = .green
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
