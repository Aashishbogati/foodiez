//
//  AllMealDealsCollectionViewCell.swift
//  Foodiez
//
//  Created by Aashish Bogati on 4/1/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class AllMealDealsCollectionViewCell: BaseCollectionViewCell {
    var mealDeal : MealDeals? {
        didSet {
            if let imageName = mealDeal?.food_image {
                thumbImageView.image = UIImage(named: imageName)
            }
            
            if let title = mealDeal?.food_name {
                titleLabel.text = title
            }
            
//            if let place = mealDeal?.restaurants.count {
//                placeLabel.text = "\(place) places"
//            }
        }
    }
    @IBOutlet weak var thumbImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var placeLabel: UILabel!
}
