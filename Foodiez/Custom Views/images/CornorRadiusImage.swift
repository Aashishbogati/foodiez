//
//  CornorRadiusImage.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/27/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class CornorRadiusImage: UIImageView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        configImageView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configImageView()
    }
    
    func configImageView() {
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
    }

}
