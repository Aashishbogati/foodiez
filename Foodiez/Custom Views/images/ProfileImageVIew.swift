//
//  ProfileImageVIew.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/31/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class ProfileImageVIew: UIImageView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        configImageView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configImageView()
    }
    
    func configImageView() {
        self.layer.cornerRadius = frame.height / 2
        self.clipsToBounds = true
    }

}
