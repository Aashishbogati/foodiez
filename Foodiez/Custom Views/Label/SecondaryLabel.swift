//
//  SecondaryLabel.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/25/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class SecondaryLabel: UILabel {

    override init(frame: CGRect) {
        super.init(frame: frame)
        configSecondaryLabel()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configSecondaryLabel()
    }
    
    func configSecondaryLabel() {
        self.font = .boldSystemFont(ofSize: 12)
        self.textColor = ColorCompatibility.secondaryLabel
    }

}
