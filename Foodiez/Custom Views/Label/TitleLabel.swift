//
//  TitleLabel.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/26/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class TitleLabel: UILabel {

    override init(frame: CGRect) {
        super.init(frame: frame)
        configTitleLabel()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configTitleLabel()
    }
    
    func configTitleLabel() {
        self.font = .boldSystemFont(ofSize: 18)
        self.textColor = Color.defaultColor
    }


}
