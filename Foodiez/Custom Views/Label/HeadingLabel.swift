//
//  HeadingLabel.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/25/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class HeadingLabel: UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configHeadingLabel()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configHeadingLabel()
    }
    
    func configHeadingLabel() {
        self.font = .systemFont(ofSize: 25, weight: UIFont.Weight(rawValue: 400))
        self.textColor = Color.defaultColor
    }

}
