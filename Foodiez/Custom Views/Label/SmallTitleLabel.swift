//
//  SmallTitleLabel.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/27/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class SmallTitleLabel: UILabel {
    override init(frame: CGRect) {
        super.init(frame: frame)
        configSmallTitleLabel()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configSmallTitleLabel()
    }
    
    func configSmallTitleLabel() {
        self.font = .boldSystemFont(ofSize: 14)
        self.textColor = ColorCompatibility.label
    }

}
