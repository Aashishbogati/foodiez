//
//  TextField.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/25/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class TextField: UITextField {

    override init(frame: CGRect) {
        super.init(frame: frame)
        configTextField()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configTextField()
    }
    
    func configTextField() {
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width, height: 1)
        bottomLine.backgroundColor = UIColor(red: 0.23529411764705882, green: 0.23529411764705882, blue: 0.2627450980392157, alpha: 0.6).cgColor
        self.borderStyle = .none
        self.layer.addSublayer(bottomLine)
    }

}
