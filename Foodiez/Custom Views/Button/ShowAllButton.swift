//
//  ShowAllButton.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/26/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class ShowAllButton: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        configButton()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configButton()
    }
    
    func configButton() {
        self.titleLabel?.font = .boldSystemFont(ofSize: 12.0)
        self.setTitleColor(ColorCompatibility.secondaryLabel, for: .normal)
    }
}
