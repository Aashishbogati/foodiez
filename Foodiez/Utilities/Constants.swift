//
//  Constants.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/31/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

typealias CollectionViewMethod = UICollectionViewDataSource & UICollectionViewDataSource & UICollectionViewDelegateFlowLayout
typealias TableViewMethod = UITableViewDelegate & UITableViewDataSource


enum API_URL {
    static var MealDeals_URL = "https://foodieznpl.herokuapp.com/application/api/common/mealDeal"
    static var MostPopularFood = "https://foodieznpl.herokuapp.com/application/api/common/popularFood"
    static var MostPopularRestaurant = "https://foodieznpl.herokuapp.com/application/api/common/popularRestaurant"
    static var Menu_URL = "https://foodieznpl.herokuapp.com/application/api/common/menu"
    static var Reviews_URL = "https://foodieznpl.herokuapp.com/application/api/common/restaurantreview"
    static var Login_URL = "https://foodieznpl.herokuapp.com/application/api/common/login"
}
