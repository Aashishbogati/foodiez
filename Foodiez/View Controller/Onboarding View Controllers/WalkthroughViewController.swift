//
//  WalkthroughViewController.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/25/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit
class WalkthroughViewController: UIViewController, WalkthroughPageViewControllerDelegate {
    
    func didUpdatePageIndex(currentIndex: Int) {
        updateUI()
    }
    
    
    @IBOutlet weak var nextButton: DefaultButton!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    
    // MARK: - Properties
    weak var walkthroughPageViewController: WalkthroughPageViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

     
    }
    
    func updateUI() {
        if let index = walkthroughPageViewController?.currentIndex {
            switch index {
            case 0...2:
                nextButton.setTitle("Next", for: .normal)
                skipButton.isHidden = false
            
            case 3:
                nextButton.setTitle("Get Started", for: .normal)
                skipButton.isHidden = true
                
            default: break
            }
            
            pageControl.currentPage = index
        }
    }
    
    //MARK:- Actions
    @IBAction func nextButtonAction(_ sender: DefaultButton) {
       if let index = walkthroughPageViewController?.currentIndex {
           switch index {
           case 0...2:
               walkthroughPageViewController?.forwardPage()
               
           case 3:
            let tabBarVC : TabBarViewController = TabBarViewController.instantiate(name: "TabBar")
            self.view.window?.rootViewController = tabBarVC
           default: break
           }
       }
       
       updateUI()
    }
    
    
    @IBAction func skipButtonAction(_ sender: UIButton) {
        let tabBarVC : TabBarViewController = TabBarViewController.instantiate(name: "TabBar")
        self.view.window?.rootViewController = tabBarVC
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination
        if let pageViewController = destination as? WalkthroughPageViewController {
            walkthroughPageViewController = pageViewController
            walkthroughPageViewController?.walkthroughDelegate = self
        }
    }
}
