//
//  WalkthroughContentViewController.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/25/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit
import Lottie

class WalkthroughContentViewController: UIViewController {
    
    //MARK:- outlets
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var subHeadingLabel: UILabel!

  
    @IBOutlet weak var animationView: AnimationView!
    
    //MARK:- properties
    var index = 0
    var heading = ""
    var subHeading = ""
    var animationFiles = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.headingLabel.text = heading
        self.subHeadingLabel.text = subHeading
        startAnimation()
    }
    
    func startAnimation() {
        self.animationView.animation = Animation.named(animationFiles)
        self.animationView.loopMode = .loop
        self.animationView.play()
    }
    

  

}
