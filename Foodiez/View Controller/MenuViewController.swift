//
//  MenuViewController.swift
//  Foodiez
//
//  Created by Aashish Bogati on 4/27/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController,TableViewMethod {
    var name = "Menu"
    var res_id : Int? {
        didSet {
            getApiData()
        }
    }
    
    var menus : Menu?
    //MARK:-
    //MARK:- PROPERTIES
    @IBOutlet weak var menuTableView: UITableView!
    
    //MARK:-
    //MARK:- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        configTableView()
        configNavigationBar()
        configTableView()
    }
    
    //MARK:-
    //MARK:- FUNCTIONS
    func configTableView() {
        menuTableView.delegate = self
        menuTableView.dataSource = self
        menuTableView.tableFooterView = UIView()
    }
    
    func configNavigationBar() {
        
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        
        let doneButton = UIButton(frame: .init(x: 0, y: 0, width: 30, height: 30))
        doneButton.setTitle("Done", for: .normal)
        doneButton.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
        doneButton.setTitleColor(Color.defaultColor, for: .normal)
        doneButton.titleLabel?.font = .boldSystemFont(ofSize: 15)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: doneButton)
    }
    
    //MARK:-
    //MARK:- NETWORKING
    func getApiData() {
        NetworkManger.shared.fetchData(urlString: "\(API_URL.Menu_URL)?name=menu&res_id=\(res_id ?? 0)", method: "GET") { (menuData : Menu) in
            if menuData.status == true {
                self.menus = menuData
                DispatchQueue.main.async {
                    self.menuTableView.reloadData()
                }
            }
        }
    }
    
    //MARK:
    //MARK:- ACTIONS
    @objc func dismissVC() {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:-
    //MARK:- DELEGATES
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = menus?.data?.items.count {
            return count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as! MenuTableViewCell
        cell.menu = menus?.data?.items[indexPath.item]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }

}
