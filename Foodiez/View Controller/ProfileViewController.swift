//
//  ProfileViewController.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/26/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//
import Foundation
import UIKit
import SDWebImage

class ProfileViewController: UIViewController,TableViewMethod {
    
    
    //MARK:-
    //MARK:- PROPERTIES
    var isLoggedIn : Bool?
    var profileSettings = ["Mange payment Options","More Settings","Logout"]
    var firstName = ""
    var lastName = ""
    var loginUser : User? {
        didSet {
            if let imageName = loginUser?.data?.image{
                let imageURL = URL(string: imageName)
                profileImage.sd_setImage(with: imageURL, completed: nil)
            }
            
            if let userfirstName = loginUser?.data?.firstname {
                firstName = userfirstName
            }
            
            if let userlastName = loginUser?.data?.lastname {
                lastName = userlastName
            }
        }
    }
    @IBOutlet weak var profileImage: ProfileImageVIew!
    @IBOutlet weak var userNameLabel: SmallTitleLabel!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:-
    //MARK:- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        checkUserStatus()
        configTableView()
        configUserName()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if let index = self.tableView.indexPathForSelectedRow{
            self.tableView.deselectRow(at: index, animated: true)
        }
    }
    
    //MARK:-
    //MARK:- FUNCTIONS
    func checkUserStatus() {
        if UserDefaults.standard.object(forKey: "User") != nil {
            let user = UserDefaults.standard.object(forKey: "User")
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: user!, options: .prettyPrinted)
                let newData = try JSONDecoder().decode(User.self, from: jsonData)
                self.loginUser = newData
            } catch {
                print(error.localizedDescription)
            }
            self.removeEmptyStateUser()
        } else {
            DispatchQueue.main.async {
                self.showEmptyStateUser()
            }
        }
    }
    
    func configUserName() {
        userNameLabel.text = "\(firstName) \(lastName)"
    }
    
    func configTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
    }
    
    //MARK:-
    //MARK:- DELEGATES
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profileSettings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileSettingsTableViewCell", for: indexPath) as! ProfileSettingsTableViewCell
        cell.titleLabel.text = profileSettings[indexPath.item]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.item == 2 {
            Alert.showLogutAlert(on: self, title: "Logout", message: "Are you sure want to logout?") { (alert) in
                UserDefaults.standard.removeObject(forKey: "User")
                DispatchQueue.main.async {
                    self.showEmptyStateUser()
                }
            }
        }
    }

}
