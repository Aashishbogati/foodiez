//
//  MostPopularViewController.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/27/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class MostPopularViewController: UIViewController,CollectionViewMethod {
    
    //MARK:
    //MARK:- PROPERTIES
    var mostPopulars : MostPopular?
    let titleLabel : UILabel = {
        let label = UILabel(frame: .init(x: 0, y: 0, width: 100, height: 100))
        label.text = "Most Popular"
        label.font = .boldSystemFont(ofSize: 20)
        label.textColor = Color.defaultColor
        return label
    }()
    
    @IBOutlet weak var collectionView: UICollectionView!
    //MARK:
    //MARK:- LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        configNavigationBar()
        configColllectionView()
        
    }
    
    //MARK:
    //MARK:- FUNCTIONS
    func configNavigationBar() {
        
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationItem.titleView = titleLabel
        
        let doneButton = UIButton(frame: .init(x: 0, y: 0, width: 30, height: 30))
        doneButton.setTitle("Done", for: .normal)
        doneButton.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
        doneButton.setTitleColor(Color.defaultColor, for: .normal)
        doneButton.titleLabel?.font = .boldSystemFont(ofSize: 15)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: doneButton)
    }
    
    func configColllectionView() {
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.minimumLineSpacing = 10

        }
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    
    //MARK:
    //MARK:- ACTIONS
    @objc func dismissVC() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK:
    //MARK:- DELEGATES
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = mostPopulars?.data.count {
            return count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MostPopularCollectionViewCell", for: indexPath) as! MostPopularCollectionViewCell
        cell.mostPopular = mostPopulars?.data[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detailsVC = UIStoryboard(name: "PopularRestaurantDetail", bundle: nil).instantiateViewController(withIdentifier: "PopularRestaurantDetailsViewController") as! PopularRestaurantDetailsViewController
        detailsVC.modalPresentationStyle = .fullScreen
        if let resturentData = mostPopulars?.data[indexPath.row] {
            DispatchQueue.main.async {
                detailsVC.mostPopular = resturentData
                detailsVC.configMostPopular()
            }
        }
        self.present(detailsVC, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: (collectionView.frame.width - 40), height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: 20, left: 20, bottom: 0, right: 20)
    }

}
