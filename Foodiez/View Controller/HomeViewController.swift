//
//  HomeViewController.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/26/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit
class HomeViewController: UIViewController, UISearchBarDelegate,CollectionViewMethod,TableViewMethod {
    
    //MARK:-
    //MARK:- MODAL DATA
    
    var cuisines : [Cuisine] = {
        let arab = Cuisine(name: "Arab", bgImage: "arab")
        let mughal = Cuisine(name: "Mughal", bgImage: "mughal")
        let latvian = Cuisine(name: "Latian", bgImage: "latvian")
        let nep = Cuisine(name: "Nepalese", bgImage: "nep")
        let chinese = Cuisine(name: "Chinese", bgImage: "chinese")
        return [arab,mughal,latvian,nep,chinese]
    }()
    
    var mostPopulars : MostPopular?
    var heroContents : [HeroContent?] = {
        let dhaba = HeroContent(thumbImage: "dhaba", name: "Dhaba style Chicken", numberOfPlace: "10 places")
        let soul = HeroContent(thumbImage: "soul", name: "Soul food", numberOfPlace: "2 places")
        let momo = HeroContent(thumbImage: "momo", name: "Momo", numberOfPlace: "21 places")
        return [dhaba,soul,momo]
    }()
    
    var mealDeals : MealDeal?
    var popularRestaurants : Restaurants?
    //MARK: -
    //MARK: PROPERTIES
    @IBOutlet weak var heroImageSliderCollectionView: UICollectionView!
    @IBOutlet weak var popularCollectionView: UICollectionView!
    @IBOutlet weak var dealsCollectionView: UICollectionView!
    @IBOutlet weak var popularRestaurantTableView: UITableView!
    @IBOutlet weak var cuisineCollectionView: UICollectionView!
    
    
    lazy var locationView : LocationView = {
        let sv = LocationView()
        return sv
    }()
    
    lazy var slideInMenuView : SlideInMenu = {
        let sm = SlideInMenu()
        return sm
    }()
    
    //MARK: -
    //MARK: LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        configNavigationBar()
        configCuisineCollectionView()
        configHeroSliderCollectionView()
        configPopularCollectionView()
        configDealsCollectionView()
        configPopularRestaurant()
        getApiData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 11.0, *) {
            navigationItem.hidesSearchBarWhenScrolling = false
        } else {
            // Fallback on earlier versions
        }
        if let index = self.popularRestaurantTableView.indexPathForSelectedRow{
            self.popularRestaurantTableView.deselectRow(at: index, animated: true)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if #available(iOS 11.0, *) {
            navigationItem.hidesSearchBarWhenScrolling = false
        } else {
            // Fallback on earlier versions
        }
    }
    
    //MARK: -
    //MARK: FUNCTIONS
    func configNavigationBar() {
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        configLeftBarButton()
        configRightBarButton()
        configSearchBar()
    }
    
    
    
    func configLeftBarButton() {
        let locationButton = UIButton(frame: .init(x: 0, y: 0, width: 30, height: 40))
        locationButton.setImage(UIImage(named: "marker")?.withRenderingMode(.alwaysTemplate), for: .normal)
        locationButton.tintColor = Color.defaultColor
        locationButton.imageEdgeInsets = .init(top: 10, left: -20, bottom: 10, right: 0)
        locationButton.imageView?.contentMode = .scaleAspectFit
        locationButton.setTitle("Kathmandu,Nepal", for: .normal)
        locationButton.titleEdgeInsets = .init(top: 0, left: -20, bottom: 0, right: 0)
        locationButton.titleLabel?.font = .boldSystemFont(ofSize: 14)
        locationButton.setTitleColor(ColorCompatibility.label, for: .normal)
        locationButton.addTarget(self, action: #selector(showLocationSetting), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: locationButton)
    }
    
    func configRightBarButton() {
        let hamButton = UIButton(frame: .init(x: 0, y: 0, width: 20, height: 20))
        hamButton.setImage(UIImage(named: "more")?.withRenderingMode(.alwaysTemplate), for: .normal)
        hamButton.tintColor = ColorCompatibility.label
        hamButton.addTarget(self, action: #selector(showSlideinMenu), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: hamButton)
    }
    
    func configSearchBar() {
        let searchController = UISearchController(searchResultsController: nil)
        if #available(iOS 9.1, *) {
            searchController.obscuresBackgroundDuringPresentation = true
        } else {
            // Fallback on earlier versions
        }
        
        searchController.searchBar.placeholder = "Search foods"
        if #available(iOS 11.0, *) {
            self.navigationItem.searchController = searchController
        } else {
            // Fallback on earlier versions
        }
        self.definesPresentationContext = true
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
        } else {
            // Fallback on earlier versions
        }
    }
    

    
    func configCuisineCollectionView() {
        if let layout = cuisineCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
    
        }
        cuisineCollectionView.showsHorizontalScrollIndicator = false
        cuisineCollectionView.delegate = self
        cuisineCollectionView.dataSource = self
    }
    
    func configHeroSliderCollectionView() {
        if let layout = heroImageSliderCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
            layout.minimumLineSpacing = 0
        }
        heroImageSliderCollectionView.showsHorizontalScrollIndicator = false
        heroImageSliderCollectionView.isPagingEnabled = true
        heroImageSliderCollectionView.delegate = self
        heroImageSliderCollectionView.dataSource = self
    }
    
    func configPopularCollectionView() {
        if let layout = popularCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
        }
        popularCollectionView.showsHorizontalScrollIndicator = false
        popularCollectionView.delegate = self
        popularCollectionView.dataSource = self
    }
    
    func configDealsCollectionView() {
        if let layout = dealsCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
        }
        dealsCollectionView.showsHorizontalScrollIndicator = false
        dealsCollectionView.delegate = self
        dealsCollectionView.dataSource = self
    }
    
    func configPopularRestaurant() {
        popularRestaurantTableView.delegate = self
        popularRestaurantTableView.dataSource = self
        popularRestaurantTableView.isScrollEnabled = false
    }
    
    
    //MARK:-
    //MARK:- NETWORKING
    func getApiData() {
        NetworkManger.shared.fetchData(urlString: API_URL.MealDeals_URL, method: "GET") { (mealDeal: MealDeal) in
            if mealDeal.status == true {
                self.mealDeals = mealDeal
                DispatchQueue.main.async {
                    self.dealsCollectionView.reloadData()
                }
            }
            
        }
        
        NetworkManger.shared.fetchData(urlString: API_URL.MostPopularFood, method: "GET") { (popularFood : MostPopular) in
            if popularFood.status == true {
                self.mostPopulars = popularFood
                DispatchQueue.main.async {
                    self.popularCollectionView.reloadData()
                }
            }
        }
        
        NetworkManger.shared.fetchData(urlString: API_URL.MostPopularRestaurant, method: "GET") { (popularRes : Restaurants) in
            if popularRes.status == true {
                self.popularRestaurants = popularRes
                DispatchQueue.main.async {
                    self.popularRestaurantTableView.reloadData()
                }
            }
        }
        
    }

    
    //MARK: -
    //MARK: ACTIONS
    @IBAction func PopularResShowAllBtnAction(_ sender: ShowAllButton) {
        
        let allRestaurntVC : AllPopularRestaurantViewController = AllPopularRestaurantViewController.instantiate(name: "AllPopularRestaurant")
        let allRestaurntNC = UINavigationController(rootViewController: allRestaurntVC)
        allRestaurntVC.popularRestaurants = popularRestaurants
        allRestaurntNC.modalPresentationStyle = .fullScreen
        self.present(allRestaurntNC, animated: true, completion: nil)
     }
    
    @objc func showLocationSetting() {
        locationView.showSetting()
    }
    
    @objc func showSlideinMenu() {
        slideInMenuView.showSettings()
    }
    
    
    @IBAction func mostPopularShowAllAction(_ sender: ShowAllButton) {
        let mostPopularVC : MostPopularViewController = MostPopularViewController.instantiate(name: "MostPopular")
        let mostPopularNC = UINavigationController(rootViewController: mostPopularVC)
        mostPopularNC.modalPresentationStyle = .fullScreen
        mostPopularVC.mostPopulars = mostPopulars
        self.present(mostPopularNC, animated: true, completion: nil)
    }
    
    
    @IBAction func mealDealsShowAllButtonActions(_ sender: ShowAllButton) {
        let allmealsVC : AllMealDealsViewController = AllMealDealsViewController.instantiate(name: "AllMealDeals")
        allmealsVC.mealDeals = mealDeals
        let allmealsNC = UINavigationController(rootViewController: allmealsVC)
        allmealsNC.modalPresentationStyle = .fullScreen
        self.present(allmealsNC,animated: true,completion: nil)
    }
    //MARK: -
    //MARK: DELEGATES
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == popularCollectionView {
            if let count = mostPopulars?.data.count {
                return count
            }
            return 0
        } else if collectionView == dealsCollectionView {
            if let count = mealDeals?.data.count {
                return count
            }
            return 0
        } else if collectionView == cuisineCollectionView {
            return cuisines.count
        } else {
            return heroContents.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == popularCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PopularCollectionViewCell", for: indexPath) as! PopularCollectionViewCell
            DispatchQueue.main.async {
                cell.popularRestaurant = self.mostPopulars?.data[indexPath.item]
            }
            
            return cell
        } else if collectionView == dealsCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DealsCollectionViewCell", for: indexPath) as! DealsCollectionViewCell
            DispatchQueue.main.async {
               cell.mealDeal = self.mealDeals?.data[indexPath.item]
            }
            
            return cell
        } else if collectionView == cuisineCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CuisineCollectionViewCell", for: indexPath) as! CuisineCollectionViewCell
            cell.cuisine = cuisines[indexPath.item]
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HeroSliderCollectionViewCell", for: indexPath) as! HeroSliderCollectionViewCell
            cell.heroContent = heroContents[indexPath.item]
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == popularCollectionView {
            return .init(width: popularCollectionView.frame.width / 1.5, height: 225)
        } else if collectionView == dealsCollectionView {
            return .init(width: dealsCollectionView.frame.width / 2.3, height: dealsCollectionView.frame.height)
        } else if collectionView == cuisineCollectionView {
            return .init(width: cuisineCollectionView.frame.width / 2.5, height: 60)
        } else {
            return .init(width: heroImageSliderCollectionView.frame.width, height: heroImageSliderCollectionView.frame.height)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == cuisineCollectionView {
            return .init(top: 0, left: 10, bottom: 10, right: 10)
        }
        return .init(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == dealsCollectionView {
            let detailsVC : MealDealsDetailViewController = MealDealsDetailViewController.instantiate(name: "MealDealsDetails")
            detailsVC.modalPresentationStyle = .fullScreen
            DispatchQueue.main.async {
                detailsVC.mealDeal = self.mealDeals?.data[indexPath.row]
            }
            
            self.present(detailsVC, animated: true, completion: nil)
        }
        
        if collectionView == popularCollectionView {
            
            let detailsVC : PopularRestaurantDetailsViewController = PopularRestaurantDetailsViewController.instantiate(name: "PopularRestaurantDetail")
            detailsVC.modalPresentationStyle = .fullScreen
            if let resturentData = mostPopulars?.data[indexPath.row] {
                DispatchQueue.main.async {
                    detailsVC.orderFoodButton.isHidden = true
                    detailsVC.mostPopular = resturentData
                    detailsVC.configMostPopular()
                }
            }
            self.present(detailsVC, animated: true, completion: nil)
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = popularRestaurants?.data.count {
            return count
        }
        
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PopularResTableViewCell", for: indexPath) as! PopularResTableViewCell
        cell.popularRestaurant = popularRestaurants?.data[indexPath.item]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailsVC : PopularRestaurantDetailsViewController = PopularRestaurantDetailsViewController.instantiate(name: "PopularRestaurantDetail")
        detailsVC.modalPresentationStyle = .fullScreen
        if let resturentData = popularRestaurants?.data[indexPath.row] {
            DispatchQueue.main.async {
                detailsVC.popularRestaurant = resturentData
                detailsVC.configPopularRestaurant()
            }
        }
        
        self.present(detailsVC, animated: true, completion: nil)
    }


}
