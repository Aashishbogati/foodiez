//
//  PopularRestaurantDetailsViewController.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/27/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit
import MapKit
import SDWebImage

class PopularRestaurantDetailsViewController: UIViewController,CollectionViewMethod {
    
    //MARK:
    //MARK:- MODEL DATA
    var popularRestaurant : Restaurants_data? {
        didSet {
            getApiData()
        }
    }
    var mostPopular : MostPopularRes? {
        didSet {
            getPopularReviews()
        }
    }

    
    //MARK:
    //MARK:- PROPERTIES
    var reviews : Reviews?
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var thumbImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var markerImage: UIImageView! {
        didSet {
            markerImage.image = UIImage(named: "marker")?.withRenderingMode(.alwaysTemplate)
            markerImage.tintColor = .white
        }
    }
    @IBOutlet weak var orderFoodButton: UIButton!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var favouriteButton: UIButton!
    @IBOutlet weak var photoButton: UIButton!
    //MARK:
    //MARK:- LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configButtons()
        configCollectionView()
        let annotations = MKPointAnnotation()
        mapView.addAnnotation(annotations)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    //MARK:
    //MARK:- FUNCTIONS
    func configCollectionView() {
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.estimatedItemSize = .init(width: 1, height: 1)
        }
        collectionView.register(UINib.init(nibName: "ReviewsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ReviewsCollectionViewCell")
        collectionView.delegate = self
        collectionView.showsVerticalScrollIndicator = false
        collectionView.dataSource = self
    }
    
    func configButtons() {
        shareButton.setTitle("Share", for: .normal)
        shareButton.setImage(UIImage(named: "home")?.withRenderingMode(.alwaysTemplate), for: .normal)
        shareButton.alignTextBelow()
        
        favouriteButton.setTitle("Favourite", for: .normal)
        favouriteButton.setImage(UIImage(named: "star")?.withRenderingMode(.alwaysTemplate), for: .normal)
        favouriteButton.alignTextBelow()
        
        photoButton.setTitle("Photos", for: .normal)
        photoButton.setImage(UIImage(named: "camera")?.withRenderingMode(.alwaysTemplate), for: .normal)
        photoButton.alignTextBelow()
    }
    
    func configPopularRestaurant() {
        if let title = popularRestaurant?.restaurant_name {
            self.titleLabel.text = title
        }
        if let resAddress = popularRestaurant?.address {
            addressLabel.text = resAddress
        }
        
//        if let reviewsData = popularRestaurant?.reviews {
//            reviews = reviewsData
//        }
        
        if let thumbimage = popularRestaurant?.restaurant_image {
            let imageURL = URL(string: thumbimage)
            thumbImage.sd_setImage(with: imageURL, completed: nil)
        }
    }
    
    func configMostPopular() {
        if let title = mostPopular?.restaurnat_name {
            self.titleLabel.text = title
        }
        if let resAddress = mostPopular?.address {
            addressLabel.text = resAddress
        }

//        if let reviewsData = mostPopular?.reviews {
//            reviews = reviewsData
//        }

        if let thumbimage = mostPopular?.food_image {
             let imageURL = URL(string: thumbimage)
             thumbImage.sd_setImage(with: imageURL, completed: nil)
        }
    }
    
    //MARK:-
    //MARK:- NETWORKING
    func getApiData() {
        let res_id = self.popularRestaurant?.restaurant_id
        NetworkManger.shared.fetchData(urlString: "\(API_URL.Reviews_URL)?name=review&res_id=\(res_id ?? 0)", method: "GET") { (reviewsData: Reviews) in
            self.reviews = reviewsData
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    func getPopularReviews() {
        let res_id = self.mostPopular?.restaurant_id
        NetworkManger.shared.fetchData(urlString: "\(API_URL.Reviews_URL)?name=review&res_id=\(res_id ?? 0)", method: "GET") { (reviewsData: Reviews) in
            self.reviews = reviewsData
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    //MARK:
    //MARK:- ACTIONS
    @IBAction func dismissButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func orderFoodAction(_ sender: UIButton) {
        let menuVC : MenuViewController = MenuViewController.instantiate(name: "Menu")
        let menuNC = UINavigationController(rootViewController: menuVC)
        menuNC.modalPresentationStyle = .overFullScreen
        menuNC.modalTransitionStyle = .crossDissolve
        DispatchQueue.main.async {
            menuVC.res_id = self.popularRestaurant?.restaurant_id
        }
        self.present(menuNC, animated: true, completion: nil)
    }
    
    @IBOutlet weak var dimissButton: UIButton! {
        didSet {
            dimissButton.setImage(UIImage(named: "cancel")?.withRenderingMode(.alwaysTemplate), for: .normal)
            dimissButton.tintColor = .white
        }
    }
    
    //MARK:-
    //MARK:- DELEGATES
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = reviews?.data?.reviews.count {
            return count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReviewsCollectionViewCell", for: indexPath) as! ReviewsCollectionViewCell
        cell.review = reviews?.data?.reviews[indexPath.row]
        return cell
    }


}


