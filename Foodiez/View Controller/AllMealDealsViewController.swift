//
//  AllMealsDealViewController.swift
//  Foodiez
//
//  Created by Aashish Bogati on 4/1/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class AllMealDealsViewController: UIViewController,CollectionViewMethod {
    
    var mealDeals : MealDeal?
    //MARK
    //MARK:- PROPERTIES
    @IBOutlet weak var collectionView: UICollectionView!
    let titleLabel : UILabel = {
        let label = UILabel(frame: .init(x: 0, y: 0, width: 100, height: 100))
        label.text = "Meal Deals"
        label.font = .boldSystemFont(ofSize: 20)
        label.textColor = Color.defaultColor
        return label
    }()
    //MARK:-
    //MARK:- LIFE CYCELE
    override func viewDidLoad() {
        super.viewDidLoad()
        configNavigationBar()
        configCollectionView()
    }
    
    //MARK
    //MARK:- FUNCTIONS
    func configNavigationBar() {
        
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationItem.titleView = titleLabel
        
        let doneButton = UIButton(frame: .init(x: 0, y: 0, width: 30, height: 30))
        doneButton.setTitle("Done", for: .normal)
        doneButton.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
        doneButton.setTitleColor(Color.defaultColor, for: .normal)
        doneButton.titleLabel?.font = .boldSystemFont(ofSize: 15)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: doneButton)
    }
    
    func configCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    //MARK:
    //MARK:- ACTIONS
    @objc func dismissVC() {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:-
    //MARK:- DELEGATES
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AllMealDealsCollectionViewCell", for: indexPath) as! AllMealDealsCollectionViewCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: collectionView.frame.width - 24, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: 0, left: 12, bottom: 0, right: 12)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detailsVC = UIStoryboard(name: "MealDealsDetails", bundle: nil).instantiateViewController(withIdentifier: "MealDealsDetailViewController") as! MealDealsDetailViewController
        detailsVC.modalPresentationStyle = .fullScreen
//        DispatchQueue.main.async {
//            detailsVC.mealDeal = self.mealDeals?.mealdeals[indexPath.item]
//        }
        
        self.present(detailsVC, animated: true, completion: nil)
    }
    
    


}
