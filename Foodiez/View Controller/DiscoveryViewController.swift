//
//  DiscoveryViewController.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/26/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class DiscoveryViewController: UIViewController,CollectionViewMethod {
    
    let titleLabel : UILabel = {
        let label = UILabel(frame: .init(x: 0, y: 0, width: 100, height: 100))
        label.text = "Discovery"
        label.font = .boldSystemFont(ofSize: 20)
        label.textColor = Color.defaultColor
        return label
    }()

    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        configCollectionView()
        configNavigationBar()
    }
    
    //MARK:-
    //MARK:- FUNCTIONS
    func configNavigationBar() {
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        navigationItem.titleView = titleLabel
    }
    
    func configCollectionView() {
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.minimumLineSpacing = 10
            layout.minimumInteritemSpacing = 10
        }
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    //MARK:-
    //MARK:- DELEGATES
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DiscoveryCollectionViewCell", for: indexPath) as! DiscoveryCollectionViewCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: collectionView.frame.width, height: 150)
    }
   

}
