//
//  WelcomeViewController.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/25/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {
    
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var welcomeHeadingLabel: HeadingLabel!
    @IBOutlet weak var termAndCondtionBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configNavigationBar()
        
        SetupAttributedText.shared.setupButton(firstString: "Already have account? ", secondString: "Login", buttonName: loginBtn)
        
        SetupAttributedText.shared.setupButton(firstString: "By clicking signup you agree to our ", secondString: "Term & Condition", buttonName: termAndCondtionBtn)
        
        SetupAttributedText.shared.setupText(firstString: "Welcome to \nFoodiez\n", secondString: "Create your account", labelName: welcomeHeadingLabel,firstTextColor: Color.defaultColor, subTextColor: ColorCompatibility.secondaryLabel, textAlignment: .center,firstTextFontSize: 25,subTextFontSize: 14)
    }
    
    @IBAction func loginButtonAction(_ sender: UIButton) {
        let loginVC : LoginViewController = LoginViewController.instantiate(name: "Login")
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    //MARK:- Functions
    func configNavigationBar() {
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        
    }
    

}
