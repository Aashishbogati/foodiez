//
//  AllPopularRestaurantViewController.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/31/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class AllPopularRestaurantViewController: UIViewController,TableViewMethod {

    //MARK:-
    //MARK:- PROPERTIES
    let titleLabel : UILabel = {
        let label = UILabel(frame: .init(x: 0, y: 0, width: 100, height: 100))
        label.text = "All Popular Restaurants"
        label.font = .boldSystemFont(ofSize: 20)
        label.textColor = Color.defaultColor
        return label
    }()
    
    @IBOutlet weak var tableView: UITableView!
    var popularRestaurants : Restaurants?
    
    //MARK:
    //MARK:- LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
       configTableView()
        configNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let index = self.tableView.indexPathForSelectedRow{
            self.tableView.deselectRow(at: index, animated: true)
        }
    }
    
    //MARK:- FUNCTIONS
    
    func configNavigationBar() {
        
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationItem.titleView = titleLabel
        
        let doneButton = UIButton(frame: .init(x: 0, y: 0, width: 30, height: 30))
        doneButton.setTitle("Done", for: .normal)
        doneButton.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
        doneButton.setTitleColor(Color.defaultColor, for: .normal)
        doneButton.titleLabel?.font = .boldSystemFont(ofSize: 15)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: doneButton)
    }
    
    func configTableView() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    //MARK:-
    //MARK:- ACTIONS
    @objc func dismissVC() {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:
    //MARK:- DELEGATES
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = popularRestaurants?.data.count {
            return count
        } else {
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllPopularRestaurantableViewCell", for: indexPath) as! AllPopularRestaurantableViewCell
        cell.popularRestaurant = popularRestaurants?.data[indexPath.item]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailsVC = UIStoryboard(name: "PopularRestaurantDetail", bundle: nil).instantiateViewController(withIdentifier: "PopularRestaurantDetailsViewController") as! PopularRestaurantDetailsViewController
        detailsVC.modalPresentationStyle = .fullScreen
        if let resturentData = popularRestaurants?.data[indexPath.row] {
            DispatchQueue.main.async {
                detailsVC.popularRestaurant = resturentData
            }
        }
        
        self.present(detailsVC, animated: true, completion: nil)
    }

  

}
