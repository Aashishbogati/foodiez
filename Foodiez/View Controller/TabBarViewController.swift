//
//  TabBarViewController.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/26/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.tintColor = Color.defaultColor
        configTabBar()
        
    }
    
    
    func configTabBar() {
        let homeVC : HomeViewController = HomeViewController.instantiate(name: "Home")
        let homeNC = UINavigationController(rootViewController: homeVC)
        homeNC.tabBarItem.title = "Home"
        homeNC.tabBarItem.image = UIImage(named: "home")
        
        
        let discoveryVC : DiscoveryViewController = DiscoveryViewController.instantiate(name: "Discovery")
        let discoveryNC = UINavigationController(rootViewController: discoveryVC)
        discoveryNC.tabBarItem.title = "Discovery"
        discoveryNC.tabBarItem.image = UIImage(named: "marker")
        
        let favouriteVC : FavouriteViewController = FavouriteViewController.instantiate(name: "Favourite")
        let favouriteNC = UINavigationController(rootViewController: favouriteVC)
        favouriteNC.tabBarItem.title = "Favourite"
        favouriteNC.tabBarItem.image = UIImage(named: "heart")
        
        let profileVC : ProfileViewController = ProfileViewController.instantiate(name: "Profile")
        let profileNC = UINavigationController(rootViewController: profileVC)
        profileNC.tabBarItem.title = "Profile"
        profileNC.tabBarItem.image = UIImage(named: "user")
        
        
        self.viewControllers = [homeNC,discoveryNC,favouriteNC,profileNC]
    }
    

    

}
