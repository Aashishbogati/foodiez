//
//  MealDealsDetailsViewController.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/31/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit
import SDWebImage

class MealDealsDetailViewController: UIViewController,TableViewMethod {
    
    
    var mealDeal : MealDeals? {
        didSet {
            
            if let imageName = mealDeal?.food_image {
                let imageUrl = URL(string: imageName)
                thumbImage.sd_setImage(with: imageUrl, completed: nil)
            }
            
            if let title = mealDeal?.food_name {
                titleLabel.text = title
            }
            
        }
    }
    //MARK:-
    //MARK:- PROPERTIES
    @IBOutlet weak var totalRestaurantsLabel: SmallTitleLabel!
    @IBOutlet weak var thumbImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var containerView: UIView! {
        didSet {
            containerView.layer.cornerRadius = 15
        }
    }
    @IBOutlet weak var dismissButton: UIButton! {
        didSet {
            dismissButton.setImage(UIImage(named: "cancel")?.withRenderingMode(.alwaysTemplate), for: .normal)
            dismissButton.tintColor = .white
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        configTableView()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK
    //MARK:- FUNCTIONS
    func configTableView() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    //MARK:
    //MARK:- ACTIONS
    @IBAction func dismissButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK
    //MARK:- DELEGATES
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MealDealsDetailTableViewCell", for: indexPath) as! MealDealsDetailTableViewCell

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailsVC = UIStoryboard(name: "PopularRestaurantDetail", bundle: nil).instantiateViewController(withIdentifier: "PopularRestaurantDetailsViewController") as! PopularRestaurantDetailsViewController
        detailsVC.modalPresentationStyle = .fullScreen
//        if let resturentData = mealDeal?.restaurants[indexPath.row] {
//            DispatchQueue.main.async {
//                detailsVC.popularRestaurant = resturentData
//                detailsVC.configPopularRestaurant()
//            }
//        }
        
        self.present(detailsVC, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    

}
