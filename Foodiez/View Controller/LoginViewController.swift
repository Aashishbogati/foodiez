//
//  ViewController.swift
//  Foodiez
//
//  Created by Aashish Bogati on 3/24/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//
import Foundation
import UIKit

class LoginViewController: UIViewController {
    var user : User?
    @IBOutlet weak var loginHeadingLabel: UILabel!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configNavigationBar()
        SetupAttributedText.shared.setupText(firstString: "Log in into your\n", secondString: "account", labelName: loginHeadingLabel,firstTextColor: Color.defaultColor,subTextColor: Color.defaultColor,textAlignment: .left,firstTextFontSize: 25,subTextFontSize: 18)
    }
    
    //MARK:- Functions
    func configNavigationBar() {
        
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        
        let doneButton = UIButton(frame: .init(x: 0, y: 0, width: 30, height: 30))
        doneButton.setTitle("Done", for: .normal)
        doneButton.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
        doneButton.setTitleColor(Color.defaultColor, for: .normal)
        doneButton.titleLabel?.font = .boldSystemFont(ofSize: 15)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: doneButton)
    }
    
    //MARK:- Actions
    @IBAction func loginButtonAction(_ sender: UIButton) {
        guard let userName = usernameTextField.text else { return }
        guard let userPassword = passwordTextField.text else { return }
        
        let params = [
            "username" : userName,
            "password" : userPassword
        ]
        NetworkManger.shared.postData(urlString: API_URL.Login_URL, method: "POST", params: params) { (userData : User) in
            if userData.status == false {
                DispatchQueue.main.async {
                    Alert.showAlert(on: self, title: "Login", message: userData.message ?? "")
                }
                
            } else {
                self.user = userData
                DispatchQueue.main.async {
                    self.dismiss(animated: true, completion: nil)
                    
                }
            }
            
        }
    }
    
    //MARK:
    //MARK:- ACTIONS
    @objc func dismissVC() {
        self.dismiss(animated: true, completion: nil)
    }

}
